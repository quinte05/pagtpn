const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();
mongoose.connect('mongodb://localhost:27017/mevn-database')
    .then(db => console.log('DB esta conectada'))
    .catch(err => console.error(err));

//Configuraciones
app.set('port', process.env.PORT || 5000);

//middlewares (Funciones)
app.use(morgan('dev'));
app.use(express.json());

// Routes
app.use('/api/tasks', require('./routes/tasks'))

// Static Files
app.use(express.static(__dirname + '/public'));


// El servidor esta escuchando por el puerto 3000
app.listen(app.get('port'), () => { 
    console.log('Server on port', app.get('port'))
});
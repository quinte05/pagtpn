const express = require('express');
const router = express.Router();

const Task = require('../models/Task');

router.get('/', async (req, res) => {
    const tasks = await Task.find();
    res.json(tasks);
    //res.send('api task esta aqui');
});

router.get('/:id', async (req, res) => {
    const task = await Task.findById(req.params.id);
    res.json(task);
})

router.post('/', async (req, res) =>{
   
    const task = new Task(req.body);
    await task.save();
    res.json({
        status: 'Task saved'
    });
});

router.put('/:id', async (req, res) => {
    //console.log(req.params);
    await Task.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Task Updated'
    });
});

router.delete('/:id', async (req, res) =>{
    await Task.findByIdAndDelete(req.params.id);
    res.json({
        status:'Task Deleted'
    });
})

module.exports = router;

/*
18 Discos externos antigolpes HD330 2TR USB 3.1 color NEGRO-ROJO O AZUL
4 Unidad SSD externo antigolpes Nand flash 3D USBB 3.2 SD600 960gb NEGRO
4 SATA III 10 TERAS WESTERN DIGITAL CAVIAR PURPLE DVR 24/7 
4 SLI RTX 2080 
*/